﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public TextMeshProUGUI countdown;
    public TextMeshProUGUI pickUpCount;

    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public PlayerMovement script;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;

    public float timeLeft = 65f;

    private void Start()
    {
        script = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player && script.count == 0)
        {
            m_IsPlayerAtExit = true;
        }
    }

    void SetCountdownText()
    {
        countdown.text = "Time Left: " + timeLeft.ToString();
    }

    void SetPickUpCountText()
    {
        pickUpCount.text = "Items Left: " + script.count.ToString();
    }

    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    void Update()
    {
        if(m_IsPlayerAtExit && script.count == 0)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
            
        }
        else if(m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }

        timeLeft -= Time.deltaTime;
        print("timeLeft:" + timeLeft);
        if(timeLeft <= 0f)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
           
        }

        SetCountdownText();
        print("Pick Ups Left:" + script.count);
        SetPickUpCountText();
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if(!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if(m_Timer > fadeDuration + displayImageDuration)
        {
            if(doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            } 
        }
    }
}
