﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public int count;
    public GameObject exitDoor;

    public TextMeshProUGUI IntroText;
    private float timeToAppear = 0f;
    private float timeToDisappear = 10f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
   

    // Start is called before the first frame update
    void Start()
    {
        //References the Animator Component
        m_Animator = GetComponent<Animator>();
        //References the rigidbody component
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        count = 10;

        
    }

    public void EnableText()
    {
        IntroText.enabled = true;
        timeToDisappear = Time.time + timeToAppear;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Create variables for Horizontal and Vertical axes
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        // Set value for movement variable
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        //Tells animator component whether or not the character is walking
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if(isWalking)
        {
            if(!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        //Calculate charachter's forward vector
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);

        //Calls LookRotation method and creates a raotation looking in the direction of the given paramenter
        m_Rotation = Quaternion.LookRotation(desiredForward);

        if(IntroText.enabled && (Time.time >= timeToDisappear))
        {
            IntroText.enabled = false;
        }
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count - 1;

            if(count == 0)
            {
                exitDoor.SetActive(false);
            }
        }
    }
}
